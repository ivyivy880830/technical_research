package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class server {

    public static void main(String[] args) {
        // write your code here
        ServerSocket server;
        Socket socket;
        DataInputStream dis;
        DataOutputStream dos;
        int[][] data = new int[4536][4];
        int[] ComA = new int[4];
        int[] PG = new int[4];
        int[] ComG = new int[4];
        int x = 0,n = 0,m,count,nc=4536,CA = 0,CB,A,B,turn=1,ComW=0;

        try {
            Scanner sc = new Scanner(System.in);
            server = new ServerSocket(1701);
            System.out.println("開始接收");
            socket = server.accept();
            System.out.println("已有客戶連線:" + socket.getInetAddress());
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());


            while(n<nc){           //當n小於資料的筆數，用這個迴圈來存所有不重複數字的資料
                for (int i=1;i<10;i++){         //第一位數為i
                    for (int j=0;j<10;j++){   //第二位數為j
                        if(i==j){         //若第一位和第二位重複
                            continue; //那第二位接下去產生
                        }
                        for (int k=0;k<10;k++){    //第三位數為k
                            if(i==k || j==k){  //若第三位和前兩位重複
                                continue;  //那第三位接下去產生
                            }
                            for (int y=0;y<10;y++){        //第四位數為y
                                if(i==y || j==y || k==y){  //若第四位和前三位重複
                                    continue;          //那第四位接下去產生
                                }
                                data[n][0]=i;  //n為筆數
                                data[n][1]=j;
                                data[n][2]=k;
                                data[n][3]=y;
                                n++;           //每存放完一筆，就加1
                            }
                        }
                    }
                }
            }

            while(CA!=4){
                x = (int) (Math.random()*nc);
                //x=random(nc);     //x用來存電腦隨機猜的是第幾筆資料
                for(int i=0;i<4;i++){
                    ComG[i]=data[x][i]; //再用ComG去存電腦中的那筆資料
                }
                String print = "電腦猜："+ComG[0]+ComG[1]+ComG[2]+ComG[3];
                System.out.println(print);//顯示電腦猜的數字
                dos.writeUTF(print);
//                System.out.print("輸入幾A幾B:");
                String text;
                text = dis.readUTF();
                String[] ab = text.split(",");
                dos.writeUTF(ab[0]+"A"+ab[1]+"B");
                CA = Integer.parseInt(ab[0]);
                CB = Integer.parseInt(ab[1]);

                if(CA+CB>4){      //倘若輸入的CA和CB大於4
                    System.out.print("輸入錯誤~");//則顯示輸入錯誤
                    dos.writeUTF("0");
                    System.out.println("電腦猜：" + ComG[0]+ComG[1]+ComG[2]+ComG[3]);//顯示電腦猜的數字
                    text = dis.readUTF();
                    ab = text.split(",");
                    dos.writeUTF(ab[0]+"A"+ab[1]+"B");
                    CA = Integer.parseInt(ab[0]);
                    CB = Integer.parseInt(ab[1]);
                }
                if(CA==4){
                    dos.writeUTF("猜對了");
                    System.out.print("猜對了");
                    server.close();
                    System.exit(0);
                }
                m=0; //m是為了要讓資料從第0筆開始搜尋，所以要歸零
                count=0;//符合的資料也從第0筆開始存，所以也要歸零
                while(m<nc){  //整個資料和電腦猜的數字做比較
                    A=0;      //重新計算要先將A和B歸零
                    B=0;
                    for(int c=0;c<4;c++){
                        for(int f=0;f<4;f++){
                            if(data[m][f]==ComG[c]){ //倘若數字一樣
                                if(c==f){   //再判斷位置是否一樣
                                    A++;      //如果一樣，則A加一
                                }else{
                                    B++;      //否則B加一
                                }
                            }
                        }
                    }
                    if(A==CA&&B==CB){  //如果電腦猜的和資料中的數據符合
                        for(int j=0;j<4;j++){
                            data[count][j]=data[m][j];//那把資料覆蓋到原本的data中
                        }
                        count++;//每放完一筆則資料筆數加一(count資料筆數
                    }
                    m++;
                }
                ComW++;  //計算電腦猜了幾次
                nc=count;//紀錄上次符合的資料筆數
                if(nc==0){ //倘若沒有任何符合的數據
                    dos.writeUTF("你作弊!!");
                    System.out.println("你作弊!!");//則資料顯示你作弊
                }
//                if(nc==1){ //倘若數據中只剩下一筆資料符合
//                    dos.writeUTF("正確了喔~~");
//                    System.out.println("正確了喔~~");//則資料顯示正確了喔
//                    CA=4;   //也代表電腦已經猜到4A了
//                }
            }
        }catch (Exception e){

        }
    }
}
