package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class client {

    public static void main(String[] args) {
        // write your code here
        Scanner sc = new Scanner(System.in);
        try {
            Socket client = new Socket("127.0.0.1", 1701);
            System.out.println("成功連到server");
            DataOutputStream out = new DataOutputStream(client.getOutputStream());
            DataInputStream in = new DataInputStream(client.getInputStream());
            System.out.println("***********遊戲開始***********");
            while (true) {
                String result = in.readUTF();
                //System.out.println(result);
                if (result.equals("0")) {
                    System.out.println("輸入錯誤!!");
                    continue;
                }else if (result.equals("1")){
                    System.out.println("------------------重新開始------------------");
                    continue;
                }else if (result.equals("2")){
                    System.out.println("------------------遊戲結束------------------");
                    System.exit(0);
                }else {

                    System.out.println(result);
                }
                System.out.println("請輸入幾A幾B:"); //顯示要玩家做什麼
                String context = sc.next();
                out.writeUTF(context);
                String r = in.readUTF();
                System.out.println(r);
                if (r.equals("猜對了")){
                    client.close();
                    System.exit(0);
                }
            }
        }
        catch (IOException e) {
            System.out.println("error");
        }
    }
}
