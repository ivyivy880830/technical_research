package com.example.a0729calculator_work;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    TextView t_equation,t_ans;
    Button b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,divided,times,less,add,equal,C,B;
    int sign,c = 0,number[],count;
    ArrayList abc = new ArrayList();
    String ans,num="",split_line[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        t_equation = findViewById(R.id.t_equation);
        t_ans = findViewById(R.id.t_ans);
        b0 = findViewById(R.id.b0);
        b1 = findViewById(R.id.b1);
        b2 = findViewById(R.id.b2);
        b3 = findViewById(R.id.b3);
        b4 = findViewById(R.id.b4);
        b5 = findViewById(R.id.b5);
        b6 = findViewById(R.id.b6);
        b7 = findViewById(R.id.b7);
        b8 = findViewById(R.id.b8);
        b9 = findViewById(R.id.b9);
        add = findViewById(R.id.add);
        less = findViewById(R.id.less);
        times = findViewById(R.id.times);
        divided = findViewById(R.id.divided);
        equal = findViewById(R.id.equal);
        C = findViewById(R.id.C);
        B = findViewById(R.id.B);
        setButton();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b0:
                num += "0";
                count = count*10+0;
                t_equation.setText(num);
                break;
            case R.id.b1:
                num += "1";
                count = count*10+1;
                t_equation.setText(num);
                break;
            case R.id.b2:
                num += "2";
                count = count*10+2;
                t_equation.setText(num);
                break;
            case R.id.b3:
                num += "3";
                count = count*10+3;
                t_equation.setText(num);
                break;
            case R.id.b4:
                num += "4";
                count = count*10+4;
                t_equation.setText(num);
                break;
            case R.id.b5:
                num += "5";
                count = count*10+5;
                t_equation.setText(num);
                break;
            case R.id.b6:
                num += "6";
                count = count*10+6;
                t_equation.setText(num);
                break;
            case R.id.b7:
                num += "7";
                count = count*10+7;
                t_equation.setText(num);
                break;
            case R.id.b8:
                num += "8";
                count = count*10+8;
                t_equation.setText(num);
                break;
            case R.id.b9:
                num += "9";
                count = count*10+9;
                t_equation.setText(num);
                break;
            case R.id.add:
                num += "+";
                sign = 1;
                t_equation.setText(num);
                break;
            case R.id.less:
                num += "-";
                sign = 2;
                t_equation.setText(num);
                break;
            case R.id.times:
                num += "*";
                sign = 3;
                t_equation.setText(num);
                break;
            case R.id.divided:
                num += "/";
                sign = 4;
                t_equation.setText(num);
                break;
            case R.id.equal:
                split_line=num.split("\\+|-|\\*|/");
                Log.e("num",num);
                Log.e("first",split_line[0]);
                Log.e("second",split_line[1]);
                for (int i=0;i<10;i++){
                    //number[i]= Integer.parseInt(split_line[i]);
                    abc.add(split_line[i]);
                }
//                first=Integer.parseInt(split_line[0]);
//                second=Integer.parseInt(split_line[1]);

                for (int j=0;j<abc.size();j++){
                    abc.add(number[j]+number[j+1]);
                    Log.e("abc", String.valueOf(abc));
                }
//                switch (sign){
//                    case 1:
//                        ans = Integer.toString(first+second);
//                        t_equation.setText(ans);
//                        num="";
//                        break;
//                    case 2:
//                        ans = Integer.toString(first-second);
//                        t_equation.setText(ans);
//                        num="";
//                        break;
//                    case 3:
//                        ans = Integer.toString(first*second);
//                        t_equation.setText(ans);
//                        num="";
//                        break;
//                    case 4:
//                        ans = Integer.toString(first/second);
//                        t_equation.setText(ans);
//                        num="";
//                        break;
//                }
                //count.setText(num);
                //count.setText(ans);
                break;
            case R.id.C :
                t_equation.setText("0");
                num="";
                break;
            case R.id.B:

                break;
        }
    }

    public void setButton (){
        b0.setOnClickListener(this);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        b6.setOnClickListener(this);
        b7.setOnClickListener(this);
        b8.setOnClickListener(this);
        b9.setOnClickListener(this);
        add.setOnClickListener(this);
        less.setOnClickListener(this);
        times.setOnClickListener(this);
        divided.setOnClickListener(this);
        equal.setOnClickListener(this);
        C.setOnClickListener(this);
        B.setOnClickListener(this);
    }


}
