package com.company;

import java.util.Scanner;

public class guess_1to100 {
    public static void main(String[] args) {
        // write your code here
        Scanner sc = new Scanner(System.in);
        addThread time = new addThread();
        int max=100,min=1;
        time.ans = (int) (Math.random()*100)+1;
        System.out.println(time.ans);
        time.start();
        System.out.print("請輸入1-100的數字:");
        time.guess = sc.nextInt();
        while (time.guess != time.ans) {
            if (time.guess > time.ans) {
                System.out.println("猜太大ㄌ!");
                max = time.guess;
                System.out.println("猜測範圍:"+min+"到"+max);
                System.out.print("重新猜測:");
                time.guess = sc.nextInt();
            } else if (time.guess < time.ans) {
                System.out.println("猜太小ㄌ哦!");
                min = time.guess;
                System.out.println("猜測範圍:"+min+"到"+max);
                System.out.print("重新猜測:");
                time.guess = sc.nextInt();
            }
        }
        System.out.println("恭喜猜中囉!");
    }

    public static class addThread extends Thread{
        int sec = 0;
        int guess = 0,ans = 0;
        public void run(){
            while (true){
                try {
                    sleep(1000);
                    sec++;
                    if (guess == ans){
                        System.out.println("遊戲結束,花了" + sec + "秒猜中");
                        System.exit(0);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
