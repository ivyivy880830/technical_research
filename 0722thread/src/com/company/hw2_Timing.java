package com.company;

import java.util.Calendar;
import java.util.Scanner;

public class hw2_Timing {
    public static void main(String[] args) {
        // write your code here
        Scanner sc = new Scanner(System.in);
        add1Thread timing = new add1Thread();
        timing.start();
        while (true) {
            System.out.println("輸入時間");
            timing.text = sc.next();
            if (timing.text.equals("stop")) {
                continue;
            }else {
                timing.s = Integer.parseInt(timing.text);
            }
        }
    }

    public static class add1Thread extends Thread{
        String text="";
        int s = 0;
        Calendar c = Calendar.getInstance();
        public void run(){
            while (true){
                try {
                    c.setTimeInMillis(System.currentTimeMillis());
                    int hour = c.get(Calendar.HOUR_OF_DAY);
                    int min = c.get(Calendar.MINUTE);
                    int sec = c.get(Calendar.SECOND);
                    if (text.equals("stop")) {
                        System.out.println("計時結束!");
                        System.exit(0);
                    }else {
                        if (s == 0) {
                            sleep(1000);
                            System.out.println(hour+":"+min+":"+sec);
                        } else {
                            sleep(1000 * s);
                            System.out.println(hour+":"+min+":"+sec);
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
