package com.company;

public class hw3_2 {
    static Count count = new Count();
    public static void main(String[] args) {
        //Count count = new Count();
        // write your code here
        thread t2 = new thread();
        thread t2_1 = new thread();
        t2.start();
        t2_1.start();
    }

    static class Count{
        int x;
    }

    public static class thread extends Thread{
        //int x = 0;
        public void run(){
            synchronized(count) {
                for (int i = 0; i < 600; i++) {
                    count.x += 1;
                    System.out.println(this.getName() + ":" + count.x);
                }
            }
        }
    }
}
