package com.example.a0730;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ListView list;
    String[] datas = {"陳品蓁","余嘉軒","張詠傑","黑嚕嚕"};
    Spinner spinner;
    String[] subject = {"英文","資料庫","程式","java","html"};
    TextView spinner_show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = findViewById(R.id.list);
        spinner = findViewById(R.id.spinner);
        spinner_show = findViewById(R.id.spinner_show);
        ArrayAdapter adapter_datas = new ArrayAdapter(this,android.R.layout.simple_list_item_1,datas);
        list.setAdapter(adapter_datas);

        ArrayAdapter<String> adapter_subject =new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,subject);
        spinner.setAdapter(adapter_subject);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            //放要做的事
                //Toast:在畫面上彈出一個短暫訊息
                Toast.makeText(MainActivity.this,"您選擇ㄌ"+datas[i],Toast.LENGTH_SHORT).show();

                //Dialog 對話框
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setTitle("提示訊息");
                builder1.setMessage("您剛剛點擊了"+datas[i]+",位於第"+(i+1)+"列");
                AlertDialog dialog1 = builder1.create();
                dialog1.show();
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //點擊了選項後要做的事
                spinner_show.setText("您目前選擇的科目是 " + subject[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //如果沒有點擊選項會做的事
            }
        });

    }
}
