package com.example.a0730_menu;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String[] teacher = {"黃貞芬","張朝旭","溫敏淦"};
    String[] job = {"副教授","副教授 兼 圖書館系統管理組長","副教授 兼 系主任"};
    String[] phone = {"037-381519","037-381520","037-381514"};
    int[] count = {0,0,0};
    int[] pic = {R.drawable.t1,R.drawable.t2,R.drawable.t3};
    View layout1;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = findViewById(R.id.list);

        //layout放到list裡面的部分
        BaseAdapter adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return teacher.length;
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                layout1= View.inflate(MainActivity.this,R.layout.layout,null); //叫出剛才建立的自訂義layout
                ImageView allimg=layout1.findViewById(R.id.img); //宣告圖片
                TextView alltv=layout1.findViewById(R.id.tv); //宣告textview
                allimg.setImageResource(pic[i]);
                alltv.setText(teacher[i]);
                return layout1;  //return這個view
            }
        };
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //放要做的事
                //Toast:在畫面上彈出一個短暫訊息
                count[i]++;
                Toast.makeText(MainActivity.this,"您選擇ㄌ"+teacher[i]+"\n職稱:"+job[i]+"\n連絡電話:"+phone[i]+"\n目前點選次數"+count[i],Toast.LENGTH_SHORT).show();
            }
        });
    }

    //menu的部分
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);//產生選單並呼叫你的menu 作為選單樣式
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.id1){
            Toast.makeText(MainActivity.this,"歡迎進入設定系統!!!",Toast.LENGTH_SHORT).show();
            return true;
        }else if (id == R.id.id2){
            Toast.makeText(MainActivity.this,"需要什摸幫助呢?",Toast.LENGTH_SHORT).show();
            return true;
        }else if (id == R.id.id3){
            Toast.makeText(MainActivity.this,"這裡是隱藏按鈕的部分",Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
