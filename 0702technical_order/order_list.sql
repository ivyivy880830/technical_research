-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2019 年 07 月 02 日 10:25
-- 伺服器版本： 10.1.38-MariaDB
-- PHP 版本： 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `order_list`
--

-- --------------------------------------------------------

--
-- 資料表結構 `inventory`
--

CREATE TABLE `inventory` (
  `ino` int(10) NOT NULL,
  `iname` varchar(10) NOT NULL,
  `inum` int(10) NOT NULL,
  `idate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `inventory`
--

INSERT INTO `inventory` (`ino`, `iname`, `inum`, `idate`) VALUES
(1, '嗨', 55, '2019-07-03'),
(2, '嗨', 19, '2019-07-11'),
(3, '12', 20, '2019-07-17');

-- --------------------------------------------------------

--
-- 資料表結構 `purchase`
--

CREATE TABLE `purchase` (
  `pno` int(10) NOT NULL,
  `pdate` date NOT NULL,
  `pnum` int(10) NOT NULL,
  `ino` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `purchase`
--

INSERT INTO `purchase` (`pno`, `pdate`, `pnum`, `ino`) VALUES
(1, '2019-07-01', 20, 2),
(2, '2019-07-10', 20, 1),
(3, '2019-07-10', 20, 1),
(4, '2019-07-17', 2, 2),
(5, '2019-07-11', 2, 2),
(6, '2019-07-03', 20, 1),
(7, '2019-07-17', 20, 3);

-- --------------------------------------------------------

--
-- 資料表結構 `shipment`
--

CREATE TABLE `shipment` (
  `sno` int(10) NOT NULL,
  `snum` int(10) NOT NULL,
  `date` date NOT NULL,
  `customer` varchar(10) NOT NULL,
  `ino` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `shipment`
--

INSERT INTO `shipment` (`sno`, `snum`, `date`, `customer`, `ino`) VALUES
(8, 2, '2019-06-10', '陳品蓁', 1),
(9, 3, '2019-06-10', '王草莓', 1),
(10, 2, '2019-06-10', '王草莓', 1),
(11, 2, '2019-06-10', '李佳蓉', 1),
(12, 2, '2019-06-10', '李佳蓉', 1),
(17, 5, '2019-07-10', '黑嚕嚕', 2),
(18, 5, '2019-07-10', '陳品蓁', 1),
(19, 5, '2019-07-10', '陳品蓁', 1);

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`ino`);

--
-- 資料表索引 `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`pno`),
  ADD KEY `ino` (`ino`);

--
-- 資料表索引 `shipment`
--
ALTER TABLE `shipment`
  ADD PRIMARY KEY (`sno`),
  ADD KEY `ino` (`ino`);

--
-- 在傾印的資料表使用自動增長(AUTO_INCREMENT)
--

--
-- 使用資料表自動增長(AUTO_INCREMENT) `inventory`
--
ALTER TABLE `inventory`
  MODIFY `ino` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `purchase`
--
ALTER TABLE `purchase`
  MODIFY `pno` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `shipment`
--
ALTER TABLE `shipment`
  MODIFY `sno` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- 已傾印資料表的限制(constraint)
--

--
-- 資料表的限制(constraint) `purchase`
--
ALTER TABLE `purchase`
  ADD CONSTRAINT `purchase_ibfk_1` FOREIGN KEY (`ino`) REFERENCES `inventory` (`ino`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 資料表的限制(constraint) `shipment`
--
ALTER TABLE `shipment`
  ADD CONSTRAINT `shipment_ibfk_1` FOREIGN KEY (`ino`) REFERENCES `inventory` (`ino`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
