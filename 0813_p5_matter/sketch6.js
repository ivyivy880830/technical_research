//電風扇
function setup() {
  createCanvas(600, 400);
}

function draw() {
  background(220);
  // ellipseMode(CENTER);
  fill(255,255,0);
  // ellipse(100,100,150,150);
  //
  // push();
  // translate(100,100);
  // fill(0,0,255);
  // ellipse(0,0,50,50);
  //
  // rotate(frameCount*0.1); //rotate旋轉角度
  // console.log(frameCount*0.1);
  // rect(0,0,50,50);
  // pop();


push(); //使用push和pop，將坐標系統恢復到原始的座標系統
  translate(150,200);
  rect(-10,0,20,100);
  rotate(frameCount*0.02); //rotate旋轉角度
  rectMode(CENTER);
  rect(0,0,20,100);

  rectMode(CENTER);
  rect(0,0,100,20);
pop();

push();
  translate(300,200);
  rect(-10,0,20,100);
  push();
  rotate(frameCount*0.1); //rotate旋轉角度
  rectMode(CENTER);
  rect(0,0,20,100);

  rectMode(CENTER);
  rect(0,0,100,20);
  pop();

}
