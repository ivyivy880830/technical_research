//Image
function preload(){
  img = loadImage('talk.jpeg');
}

function setup() {
  createCanvas(600, 500);
}

function draw() {
  background(220);
  push();
  image(img,100,100,img.width/20,img.height/20);
  scale(6); //放大2倍
  translate(-100,-100);
  image(img,100,100,img.width/20,img.height/20);
  pop();

  image(img,100,100,img.width/20,img.height/20);
  scale(6); //放大2倍
  translate(-50,-100);
  image(img,100,100,img.width/20,img.height/20);
}
