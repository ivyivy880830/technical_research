//3D 圖型
function setup() {
  createCanvas(600, 400,WEBGL);
}

function draw() {
  background(220);
  fill(0,0,255);
  translate(25,25);
  push();
  rotateX(50);
  rotateY(50);
  rotateZ(600);
  rotateY(frameCount*0.02); //rotate旋轉角度
  cone(60,100);
  camera(0,-50,mouseX,0,0,0,0,1,0);
  pop();
  translate(0,60);
  box(20,70,20);

}
