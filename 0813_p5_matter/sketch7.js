//圍繞星球
function setup() {
  createCanvas(800, 600);
}

function draw() {
  background(220);
  fill('#888888');
  translate(400,300);
  ellipse(0,0,50,50);

  fill(0,0,255);
  rotate(frameCount*0.02); //rotate旋轉角度
  ellipse(100,100,30,30);

  translate(100,100);
  fill(0,255,255);
  rotate(frameCount*0.1); //rotate旋轉角度
  ellipse(50,50,10,10);

}
