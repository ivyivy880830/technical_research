function Box(x,y,w,h){
  var options={friction:0.3,restitution:1,}
  this.body = Bodies.rectangle(x,y,w,h,options);
  this.w = w;
  this.h = h;
  World.add(world,this.body);

  this.show = function(){
    var pos = this.body.position;
    var angle = this.body.position;
    push();
      translate(pos.x,pos.y);
      rotate(angle);
      rectMode(CENTER);
      rect(0,0,this.w,this.h);
    pop();
  }
}

var Engine = Matter.Engine,
    World = Matter.World,
    Bodies = Matter.Bodies;
var boxB =[];

function setup() {
  createCanvas(800, 600);
  engine = Engine.create();
  world = engine.world;
  options={isStatic:true}

  // boxB = Bodies.rectangle(400,200,20,20);
  ground = Bodies.rectangle(400,600,20,20,options);
  Engine.run(engine);
  World.add(world,boxB);
  World.add(world,ground);
}

function mouseDragged() {
  boxB.push(new Box(mouseX,mouseY,random(20,30),random(20,30)));
}

function draw() {
  background(125);
  rectMode(CENTER);
  rect(200,height,width,10);
  for (var i = 0; i < boxB.length; i++) {
    boxB[i].show();
  }
  // rect(boxa.position.x,boxa.position.y,20,20);
  // rect(ground.position.x,ground.position.y,20,20);
}
