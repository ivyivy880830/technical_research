//曲線 & 圖型
function setup() {
  createCanvas(600, 400);
}

function draw() {
  background(220);
  fill(255,255,255);
  // bezier(100, 100, 150, 200, 350, 200, 400, 100);
  //
  // translate(100,100);
  // curve(100,0,100,100,40,100,350,0);

  //階梯的圖形
  beginShape();
  vertex(100,100);
  vertex(100,140);
  vertex(140,140);
  vertex(140,60);
  vertex(60,60);
  vertex(60,100);
  endShape(CLOSE);

  //正八邊形的圖形
  translate(200,200);//改位置
  beginShape();
  for (var i = 0; i < TWO_PI+1; i+=TWO_PI/8) {
    x = cos(i)*50;
    y = sin(i)*50;
    vertex(x,y);
  }
  endShape(CLOSE);

}
