//產生input、button
function setup() {
  noCanvas();
  // background('grey');
  div = createDiv('');
  div.style('background','rgb(34,34,45,0.1)');
  word = createP('請輸入字體大小!');
  inp = createInput('');
  color = createColorPicker('#ff0000');
  button = createButton('click me');

  word.id("size");
  div.child(word);
  word.child(inp);
  word.child(color);
  word.child(button);
  word.attribute('align','left');

  inp.input(myInputEvent);
  inp.style('margin-left','150px');
  inp.style('width','150px');
  inp.attribute('align','left');

  button.mousePressed(setword);
}

function myInputEvent(){
  console.log(this.value());
}

function setword(){
  s = inp.value();
  word.style('font-size',s+'px');
  word.style('color',color.value());
  console.log('you are typing:',this.value());
}

function draw() { //繪圖程式碼，每1/30秒更新一次
  background(220);
}
