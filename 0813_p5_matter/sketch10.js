//錄音
function setup() {
  createCanvas(600, 500);
  mic = new p5.AudioIn();
  mic.start();
}

function draw() {
  background(220);
  let vol = mic.getLevel();
  fill(127);
  stroke(0);
  let h = map(vol,0,1,height,0);
  ellipse(width/2,h-25,50,50);
}
