package com.company;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class stack_queue {

    public static void main(String[] args) {
        // write your code here
        ArrayList<String> arr = new ArrayList<String>();
        LinkedList<String> link = new LinkedList<String>();
        int turn=0;
        String text;
        System.out.println("請輸入要存的字串:(輸入0開始印出)");
        Scanner sc = new Scanner(System.in);
        while (turn == 0){
            text = sc.next();
            arr.add(text);
            link.add(text);
            if(text.equals("0")){
                turn = 1;
            }
        }
        System.out.println("佇列:");
        for (int i=0;i<arr.size()-1;i++){
            System.out.println(arr.get(i));
        }
        System.out.println("--------------------------");
        System.out.println("堆疊:");
        for (int i=link.size()-2;i>=0;i--){
            System.out.println(link.get(i));
        }

        //利用for each列出所有元素
        /*for (String s : arr){
            System.out.println(s);
        }*/
    }
}
