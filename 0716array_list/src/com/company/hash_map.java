package com.company;

import java.util.HashMap;
import java.util.Scanner;

public class hash_map {
    static HashMap<String,String> hashMap = new HashMap<String,String>();
    public static void main(String[] args) {
        // write your code here

        hashMap.put("東京迪士尼","35°37N,139°52E");
        hashMap.put("台北101","25°02N,121°33E");
        hashMap.put("法國艾菲爾鐵塔","48°51N,2°17E");
        hashMap.put("美國黃石國家公園","44°36N,110°30W");

        hash_map h = new hash_map();
        address add =h.new address();

        System.out.println("請輸入兩個地點:");
        Scanner sc = new Scanner(System.in);
        String a = sc.next();
        String b = sc.next();
        add.map(hashMap.get(a),hashMap.get(b));

        System.out.println("請輸入要查詢經緯度的地點:");
        String place = sc.next();
        System.out.println(hashMap.get(place));
    }

    class address {
        public void map(String x, String y) {
            String[] cut_x = x.split(",");
            String[] num_x = cut_x[1].split("°");
            String[] cut_y = y.split(",");
            String[] num_y = cut_y[1].split("°");

            int ans_x = Integer.parseInt(num_x[0]);  //字串轉數字
            int ans_y = Integer.parseInt(num_y[0]);  //字串轉數字
            if(ans_y>ans_x){
                int t=ans_x;
                ans_x=ans_y;
                ans_y=t;
            }
            int result = Math.abs(ans_x-ans_y);
            int ans = result/15;
            System.out.println("差" + ans + "個小時");
        }
    }
}
