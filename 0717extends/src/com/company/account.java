package com.company;

import java.util.Scanner;

public class account {
    static Scanner sc = new Scanner(System.in);
    public static class Account{
        double balance;
        public void credit(int m){
            balance = balance + m;
            System.out.println("存款成功");
            System.out.println("金額為" + balance);
        }
        public void debit(int n){
            balance = balance - n;
            System.out.println("提款成功");
            System.out.println("剩餘金額為" + balance);
        }
    }

    public static class SavingAccount extends Account{
        double interestRate;
        double in;
        public double setInterestRate(){
            System.out.print("設定利率:");
            interestRate = sc.nextDouble();
            return interestRate;
        }
        public double calculateInterest(int x){
            in = interestRate*x;
            //System.out.println(in);
            return in;
        }
    }

    public static class CheckAccount extends Account{
        double transactionFee,fee;
        public double chargeFee(int y){
            transactionFee =0.01;
            fee = transactionFee*y;
            System.out.println("手續費為" + fee);
            return fee;
        }
    }

    public static void main(String[] args) {
        // write your code here
        int turn=0,count;
        Account a = new Account();
        SavingAccount s = new SavingAccount();
        CheckAccount c = new CheckAccount();
        while (turn == 0) {
            System.out.println("-----------------------------");
            System.out.println("1為存款,2為提款,0為結束");
            System.out.print("請輸入要做的事項:");
            int want, money;
            want = sc.nextInt();
            switch (want) {
                case 0:
                    System.out.println("謝謝光臨!");
                    turn=1;
                    break;
                case 1:
                    System.out.print("請輸入存款金額:");
                    money = sc.nextInt();
                    System.out.print("儲蓄帳戶請輸入1,支票帳戶請輸入2:");
                    count = sc.nextInt();
                    if (count == 1){
                        s.setInterestRate();
                        s.calculateInterest(money);
                        money = money + (int)s.calculateInterest(money);
                    }else {
                        money = money - (int)c.chargeFee(money);
                    }
                    a.credit(money);
                    break;
                case 2:
                    System.out.print("請輸入提款金額:");
                    money = sc.nextInt();
                    money = money + (int)c.chargeFee(money);
                    //System.out.println(money);
                    a.debit(money);
                    break;

            }
        }
    }
}
