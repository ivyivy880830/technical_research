package com.company;

//import java.util.Arrays;
//import java.util.Comparator;
import java.util.*;

class accounts implements Comparable<accounts> {
    public String name;
    public int balance;
    accounts(String n, int b) {
        this.name = n;
        this.balance = b;
    }

    @Override
    public String toString() {
        return String.format("account(%s,%d)", name, balance);
    }

    @Override
    public int compareTo(accounts s) {
        return this.balance - s.balance;
    }
}
public class sort_account {
    public static void main(String[] args) {
        // write your code here
        //ArrayList<account> arr =new ArrayList<account>();
//        account a = new account();
        List<accounts> arr = Arrays.asList(
            new accounts("Mary",30),
            new accounts("Gary",78),
            new accounts("Tery",66),
            new accounts("Ben",1200),
            new accounts("Alice",18)
        );
        Collections.sort(arr);
        System.out.println(arr);
        arr.sort(Comparator.comparing(p->p.name));
        System.out.println(arr);
    }
}

