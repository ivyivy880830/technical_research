package com.company;

public class animal {
    public static abstract class Animal{
        String name;
        public Animal(){
            name = "animal";
        }
        public void setName(String n){
            name = n;
        }
        public abstract void move();
        public abstract void sound();
    }

    public static void main(String[] args) {
        // write your code here
        Object obj = new Animal(){
            public void move(){
                System.out.println("move:walk");
            }
            public void sound(){
                System.out.println("sound:rrrrr");
            }
        };
        ((Animal) obj).setName("cat");
        ((Animal) obj).move();
        ((Animal) obj).sound();
    }
}
